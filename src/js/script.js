document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.navbar__action');
    let overlay = document.querySelector('.overlay');

    let interact = () => {
        btn.classList.toggle('navbar__action-open');
        document.querySelector('.navbar__list').classList.toggle('navbar__list-open');
        overlay.classList.toggle('overlay_hidden');
    };

    overlay.onclick = interact;
    btn.onclick = interact;
});