const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    autoprefixer = require('gulp-autoprefixer'),
    jsMinify = require('gulp-js-minify'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    cleanCSS = require('gulp-clean-css'),
    cleanAll = require('gulp-clean'),
    concat = require('gulp-concat'),
    mergeCSS = require('merge-stream');

const path = {
    dist: {
        html: 'dist',
        css: 'dist/css',
        js: 'dist/js',
        img: 'dist/img',
        self: 'dist'
    },

    src: {
        html: 'src/*.html',
        scssBuild: 'src/scss/*.*',
        scssWatcher: 'src/scss/**/*.*',
        resetCSS: 'node_modules/reset-css/reset.css',
        fontAwesome: {
            css: 'node_modules/@fortawesome/fontawesome-free/css/all.css',
            js: 'node_modules/@fortawesome/fontawesome-free/js/all.js',
        },
        jquery: 'node_modules/jquery/dist/jquery.js',
        js: 'src/js/*.js',
        img: 'src/img/**/**/*.*',
    }
};

//----- Functions -----//

const htmlBuild = () => (
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(browserSync.stream())
);

const cssBuild = () => {
    const vendorStream = gulp.src([path.src.resetCSS, path.src.fontAwesome.css]);
    const customStream = gulp.src(path.src.scssBuild)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['>0.001%', 'last 100 versions']));
    return mergeCSS(vendorStream, customStream)
        .pipe(cleanCSS())
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
};

const jsBuild = () => (
    gulp.src([path.src.jquery, path.src.fontAwesome.js, path.src.js])
        .pipe(concat('scripts.min.js'))
        .pipe(jsMinify())
        .pipe(uglify())
        .pipe(gulp.dest(path.dist.js))
        .pipe(browserSync.stream())
);
const imgBuild = () => (
    gulp.src(path.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())
);
const cleanDist = () => (
    gulp.src(path.dist.self, {allowEmpty: true})
        .pipe(cleanAll())
        .pipe(browserSync.stream())
);


// ----- watcher -----//

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: './dist'
        },
    });
    gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
    gulp.watch(path.src.scssWatcher, cssBuild).on('change', browserSync.reload);
    gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
    gulp.watch(path.src.img, imgBuild).on('change', browserSync.reload);
};

// ----- tasks -----//

gulp.task('build', gulp.series(
    cleanDist,
    htmlBuild,
    cssBuild,
    imgBuild,
    jsBuild
));

gulp.task('dev', gulp.series(
    'build',
    watcher
));


